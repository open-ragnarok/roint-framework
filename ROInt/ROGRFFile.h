//
//  ROGRFFile.h
//  ROInt
//
//  Created by Luis Sergio Saboia Moura Junior on 4/12/11.
//  Copyright 2011 IceTempest. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ROGRF;

@interface ROGRFFile : NSObject {
    struct ROGrfFile *file;
    ROGRFFile *parent;
}

-(int)size;
-(NSData*)data;

// Returns a pointer to the character string data as presented on the grf file
-(const char*)fn;

// Returns the UTF-8 value of the filename
-(NSString*)Filename;

@end
