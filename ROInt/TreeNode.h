//
//  TreeNode.h
//  ROInt
//
//  Created by Luis Sergio Saboia Moura Junior on 4/12/11.
//  Copyright 2011 IceTempest. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface TreeNode : NSObject {
    unsigned int idx;
    
    TreeNode *right;
    TreeNode *left;
}

@property(nonatomic, assign) unsigned int idx;

@property(nonatomic, retain) TreeNode *right;
@property(nonatomic, retain) TreeNode *left;


@end
