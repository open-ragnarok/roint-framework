//
//  GRF.m
//  ROInt
//
//  Created by Luis Sergio Saboia Moura Junior on 4/12/11.
//  Copyright 2011 IceTempest. All rights reserved.
//

#import "ROGRF.h"
#import "ROGRFFile.h"
#import "TreeNode.h"

#import "OpenRO-ROInt/grf.h"

// ROGRFFile improvement
@interface ROGRFFile (Initialization)
-(id)initWithFile:(struct ROGrfFile*)_file Parent:(ROGRF*)_grf;
@end

@implementation ROGRFFile (Initialization)
-(id)initWithFile:(struct ROGrfFile*)_file Parent:(ROGRF*)_grf {
    self = [super init];
    if (self) {
        file = _file;
        parent = [_grf retain];
    }
    return(self);
}
@end

// TreeNode GRF Methods
// -- We use a binary tree to search for filenames (much) faster
@interface TreeNode (SearchCompare)

-(const char*)nameFromGrf:(ROGRF*)grf;
-(int)compareWith:(const TreeNode*)node andGRF:(ROGRF*)grf;
-(void)addNode:(TreeNode*)node andGRF:(ROGRF*)grf;

@end

@implementation TreeNode (SearchCompare)

-(const char*)nameFromGrf:(ROGRF*)grf {
    return([[grf fileAtIndex:idx] fn]);
}

-(int)compareWith:(const TreeNode*)node andGRF:(ROGRF*)grf {
    return(strcmp([self nameFromGrf:grf], [node nameFromGrf:grf]));
}

-(int)compareWithString:(const char*)fn andGRF:(ROGRF*)grf {
    return(strcmp([self nameFromGrf:grf], fn));
}


-(void)addNode:(TreeNode*)node andGRF:(ROGRF*)grf {
    if ([self compareWith:node andGRF:grf] > 0) {
        if (self.right != nil) {
            [self.right addNode:node andGRF:grf];
        }
        else {
            self.right = node;
        }
    }
    else {
        if (self.left != nil) {
            [self.left addNode:node andGRF:grf];
        }
        else {
            self.left = node;
        }
    }
}

@end


// ROGRF
@implementation ROGRF

@synthesize filecount;

- (id)init {
    self = [super init];
    if (self) {
        grf = NULL;
        root = nil;
    }
    
    return self;
}

- (void)dealloc {
    [self close];
    
    [super dealloc];
}

-(void)close {
    if (grf == NULL)
        return;
    
    [root release];
    root = nil;
    
    grf_close(grf);
    grf = NULL;
}

-(void)populateTree {
    unsigned int i;

    root = [[TreeNode alloc] init];
    root.idx = 0;

    for (i = 1; i < filecount; i++) {
        TreeNode *node = [[TreeNode alloc] init];
        node.idx = i;
        [root addNode:node andGRF:self];
        [node release];
    }
}

-(bool)load:(NSString*)filename {
    if (grf != NULL)
        grf_close(grf);
    
    grf = grf_open([filename cStringUsingEncoding:NSUTF8StringEncoding]);

    if (grf == NULL)
        return(NO);

    filecount = grf_filecount(grf);
    [self populateTree];
    
    return(YES);
}

-(ROGRFFile*)fileWithName:(const char*)fn {
    TreeNode *curnode = root;
    int r;
    
    r = [curnode compareWithString:fn andGRF:self];
    while (r != 0) {
        if (r > 0)
            curnode = curnode.right;
        else
            curnode = curnode.left;
        
        if (curnode == nil)
            return(nil);
        
        r = [curnode compareWithString:fn andGRF:self];
    }
    
    return([self fileAtIndex:curnode.idx]);
    
}

-(ROGRFFile*)fileAtIndex:(unsigned int)idx {
    if (idx >= self.filecount)
        return(nil);
    
    struct ROGrfFile *file;
    ROGRFFile *ret;
    
    file = &grf->files[idx];
    ret = [[ROGRFFile alloc] initWithFile:file Parent:self];
    
    return([ret autorelease]);
}

@end
