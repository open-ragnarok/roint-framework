//
//  NSString+CP949.m
//  ROInt
//
//  Created by Luis Sergio Saboia Moura Junior on 4/12/11.
//  Copyright 2011 IceTempest. All rights reserved.
//

#import "NSString+CP949.h"

#include "OpenRO-ROInt/roint.h"
#include "OpenRO-ROInt/rostrings.h"

@implementation NSString (CP949)

+(NSString*)StringFromEUCKRData:(const char*)euc {
    roint_free_func ___free = get_roint_free_func();
    char *x = stringFromEUCKR(euc);
    if (x == NULL)
        return(nil);
    
    NSString *ret = [NSString stringWithUTF8String:x];
    ___free(x);
    
    return(ret);
}

@end
