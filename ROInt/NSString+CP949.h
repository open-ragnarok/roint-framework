//
//  NSString+CP949.h
//  ROInt
//
//  Created by Luis Sergio Saboia Moura Junior on 4/12/11.
//  Copyright 2011 IceTempest. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface NSString (CP949)

+(NSString*)StringFromEUCKRData:(const char*)euc;

@end
