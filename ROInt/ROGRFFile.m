//
//  ROGRFFile.m
//  ROInt
//
//  Created by Luis Sergio Saboia Moura Junior on 4/12/11.
//  Copyright 2011 IceTempest. All rights reserved.
//

#import "ROGRFFile.h"
#import "NSString+CP949.h"

#include "OpenRO-ROInt/grf.h"

@implementation ROGRFFile

- (id)init {
    self = [super init];
    if (self) {
        file = NULL;
        parent = nil;
    }
    
    return self;
}

- (void)dealloc {
    [super dealloc];
}

-(int)size {
    return(file->uncompressedLength);
}

-(NSData*)data {
    NSData *ret;
    if (file->data == NULL) {
        grf_getdata(file);
        ret = [NSData dataWithBytes:file->data length:file->uncompressedLength];
        grf_freedata(file);
    }
    else {
        ret = [NSData dataWithBytes:file->data length:file->uncompressedLength];
    }
    
    return(ret);
}

-(const char*)fn {
    return(file->fileName);
}

-(NSString*)Filename {
    return([NSString StringFromEUCKRData:file->fileName]);
}

@end
